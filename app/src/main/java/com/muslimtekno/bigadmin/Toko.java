package com.muslimtekno.bigadmin;

/**
 * Created by imamudin on 12/01/17.
 */

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.muslimtekno.bigadmin.app.MyAppController;
import com.muslimtekno.bigadmin.config.GlobalConfig;
import com.muslimtekno.bigadmin.mysp.ObscuredSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Hashtable;
import java.util.Map;

public class Toko extends AppCompatActivity {
    Button btn_setLokasi, btn_kirim;
    TextView tv_nama_toko, tv_poin, tv_alamat, tv_koordinat, tv_plafon, tv_nama_sales, tv_jatuh_tempo_bayar;
    ObscuredSharedPreferences pref;
    ProgressDialog loading;
    JsonObjectRequest request;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 101;
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toko);

        init();
    }
    private void init(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Toko");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(Toko.this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );

        btn_setLokasi   = (Button)findViewById(R.id.btn_setLokasi);
        btn_kirim       = (Button)findViewById(R.id.btn_kirim);

        tv_alamat       = (TextView)findViewById(R.id.tv_alamat);
        tv_nama_toko    = (TextView)findViewById(R.id.tv_nama_toko);
        tv_nama_sales   = (TextView)findViewById(R.id.tv_nama_sales);
        tv_poin         = (TextView)findViewById(R.id.tv_poin);
        tv_jatuh_tempo_bayar= (TextView)findViewById(R.id.tv_jatuh_tempo_bayar);
        tv_koordinat    = (TextView)findViewById(R.id.tv_koordinat);
        tv_plafon       = (TextView)findViewById(R.id.tv_plafon);

        tv_nama_toko.setText(pref.getString(GlobalConfig.GTOKO_NAMA,""));
        tv_alamat.setText(pref.getString(GlobalConfig.GTOKO_ALAMAT,""));
        tv_poin.setText(pref.getString(GlobalConfig.GTOKO_POIN,""));
        tv_nama_sales.setText(pref.getString(GlobalConfig.GTOKO_NAMA_SALES,""));
        tv_jatuh_tempo_bayar.setText(pref.getString(GlobalConfig.GJATUHTEMPOBAYAR,"")+" hari");
        tv_koordinat.setText(pref.getString(GlobalConfig.GTOKO_LONG,"")+";"+pref.getString(GlobalConfig.GTOKO_LAT,""));
        tv_plafon.setText(decimalToRupiah(Double.parseDouble(pref.getString(GlobalConfig.GREAL_PLAFON,"0"))));


        btn_setLokasi.setOnClickListener(btnClick);
        btn_kirim.setOnClickListener(btnClick);
    }

    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent open = null;
            if(v==btn_setLokasi){
                //startLokasi();
                //check permission lokasi untuk marshmallow
                if (ContextCompat.checkSelfPermission(Toko.this,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    //request permission camera
                    ActivityCompat.requestPermissions(Toko.this,
                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);
                } else {
                    startLokasi();
                }
                return;
                //open = new Intent(Toko.this, InputStok.class);
            }else if (v==btn_kirim){
                kirim_lokasi();
            }
            if(open != null){
                startActivity(open);
            }
        }
    };
    @Override
    public void onRequestPermissionsResult
            (int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                Log.i("Camera", "G : " + grantResults[0]);
                // If request is cancelled, the result arrays are empty.
//                Log.i(getLocalClassName(), "grantResults.length : "+grantResults.length+" grantResults[0] :  "+grantResults[0]+"");
//                Log.i(getLocalClassName(), "PackageManager.PERMISSION_GRANTED : "+PackageManager.PERMISSION_GRANTED+" PackageManager.PERMISSION_DENIED :  "+PackageManager.PERMISSION_DENIED);

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Log.i(getLocalClassName(), "akses lokasi diizinkan");
                    startLokasi();
                    return;
                } else {
                    Log.d(getLocalClassName(), "akses lokasi tidak diizinkan");

                    //It will return false if the user tapped “Never ask again”.
                    boolean is_Neveraskagain = ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.CAMERA);
                    if (is_Neveraskagain) {
                        //showAlert();
                        Log.i(getLocalClassName(), "is_Neveraskagain false");
                    } else if (!is_Neveraskagain) {
                        Log.i(getLocalClassName(), "is_Neveraskagain true");
                        if(!pref.getBoolean(GlobalConfig.PERMISIION_LOCATION_FIRST_TIME, false)){
                            //Log.i(getLocalClassName(), "PERMISIION_CAMERA_FIRST_TIME : false");
                            pref.edit().putBoolean(GlobalConfig.PERMISIION_LOCATION_FIRST_TIME, true).commit();
                        }else{
                            //Log.i(getLocalClassName(), "PERMISIION_CAMERA_FIRST_TIME : true");
                            startInstalledAppDetailsActivity(Toko.this);
                        }
                    }
                    return;
                }
            }
        }
    }
    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }
    public void startLokasi(){
        Intent i_toko = new Intent(Toko.this, CariLokasi.class);
        startActivityForResult(i_toko, GlobalConfig.KODE_CARILOKASI);
    }
    private String decimalToRupiah(double harga){
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');

        kursIndonesia.setDecimalFormatSymbols(formatRp);

        String hasil = kursIndonesia.format(harga);
        return hasil.substring(0, (hasil.length()-3));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //menu.add(1, 2, 1, "Hapus Filter").setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void notifikasi_dialog(String title, String message){
        if(title.equals("")){
            title = getResources().getString(R.string.app_name);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(Toko.this);
        builder.setTitle(title)
                .setCancelable(false)
                .setMessage(message)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GlobalConfig.KODE_CARILOKASI && resultCode == RESULT_OK && data != null) {
            float longitude = (float) Math.round(data.getDoubleExtra(GlobalConfig.G_LONGITUDE,0) * 1000000) / 1000000;
            float latitude = (float) Math.round(data.getDoubleExtra(GlobalConfig.G_LATITUDE,0) * 1000000) / 1000000;

            tv_koordinat.setText(longitude+";"+latitude);

            pref.edit().putString(GlobalConfig.GTOKO_LAT, ""+latitude).commit();
            pref.edit().putString(GlobalConfig.GTOKO_LONG, ""+longitude).commit();

            //Log.d(GlobalConfig.TAG+"lokasiku", longitude+";"+latitude);

            Toast.makeText(getApplicationContext(), "Koordinat telah perbarui. Silakan simpan.", Toast.LENGTH_SHORT).show();
        }
    }
    private void kirim_lokasi(){
        Log.d(GlobalConfig.TAG, "make progress dialog");
        loading = ProgressDialog.show(Toko.this, "Mengupdate Koordinat", "Mohon tunggu...", true);

        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_UPDATEKOORDINATE;
        //Log.d(GlobalConfig.TAG,""+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));       //mewaliki nik juga
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            jsonBody.put(GlobalConfig.ID_TOKO, pref.getString(GlobalConfig.ID_TOKO, ""));
            jsonBody.put(GlobalConfig.GTOKO_LAT, pref.getString(GlobalConfig.GTOKO_LAT, ""));
            jsonBody.put(GlobalConfig.GTOKO_LONG, pref.getString(GlobalConfig.GTOKO_LONG, ""));

            //Log.d(GlobalConfig.TAG,""+jsonBody.toString());
            request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    Log.d(GlobalConfig.TAG, "tutup loading");
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            //hapus semua preferences
                            //update database dulu, kemudian baru keluar
                            pref.edit().remove(GlobalConfig.IS_TOKO).commit();
                            pref.edit().remove(GlobalConfig.ID_TOKO).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_NAMA).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_ALAMAT).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_PIMPINAN).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_NOTELP).commit();
                            pref.edit().remove(GlobalConfig.GREAL_PLAFON).commit();
                            pref.edit().remove(GlobalConfig.GKDSALES).commit();
                            pref.edit().remove(GlobalConfig.GJATUHTEMPOBAYAR).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_LAT).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_LONG).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_NIK).commit();
                            pref.edit().remove(GlobalConfig.GTOKO_POIN).commit();

                            tutupActivity();
                        }else{
                            notifikasi_dialog("",message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if(response != null && response.data != null){
                        Log.d(GlobalConfig.TAG, "code"+response.statusCode);
                        switch(response.statusCode){
                            case 404:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 500:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                displayMessage("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                        }
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void tutupActivity(){
        AlertDialog.Builder builder = new AlertDialog.Builder(Toko.this);
        // Set the dialog title
        builder.setTitle(getResources().getString(R.string.app_name))
                .setMessage("Lokasi terbaru telah ditambahkan.\nTerima kasih.")
                .setCancelable(false)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        openDashboard();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    public void displayMessage(String toastString){
        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelRequest();
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
    }
    public void openDashboard(){
        Intent dashboard = new Intent (Toko.this, Dashboard.class);
        startActivity(dashboard);
        Toko.this.finish();
    }
}