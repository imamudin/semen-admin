package com.muslimtekno.bigadmin;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.muslimtekno.bigadmin.app.MyAppController;
import com.muslimtekno.bigadmin.config.GlobalConfig;
import com.muslimtekno.bigadmin.mysp.ObscuredSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by imamudin on 03/03/17.
 */
public class Dashboard extends AppCompatActivity {
    ObscuredSharedPreferences pref;
    ProgressDialog loading;
    JsonObjectRequest request;
    LinearLayout ll_scan, ll_keluar, ll_set_lokasi;

    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);

        init();
    }
    private void init(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.app_name));
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);

        pref = new ObscuredSharedPreferences(Dashboard.this,
                this.getSharedPreferences(GlobalConfig.NAMA_PREF, Context.MODE_PRIVATE) );

        if(!pref.getBoolean(GlobalConfig.IS_LOGIN, false)){
            openLogin();
            Log.d("login", "buka main");
        }

        ll_scan         = (LinearLayout)findViewById(R.id.ll_scan);
        ll_keluar       = (LinearLayout)findViewById(R.id.ll_keluar);
        ll_set_lokasi   = (LinearLayout)findViewById(R.id.ll_set_lokasi);

        ll_scan.setOnClickListener(btnClick);
        ll_keluar.setOnClickListener(btnClick);
        ll_set_lokasi.setOnClickListener(btnClick);

    }

    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent open = null;
            if(v==ll_scan){
                //check permission camera untuk marshmallow
                if (ContextCompat.checkSelfPermission(Dashboard.this,
                        Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
                    //request permission camera
                    ActivityCompat.requestPermissions(Dashboard.this,
                            new String[]{Manifest.permission.CAMERA},
                            MY_PERMISSIONS_REQUEST_CAMERA);
                } else {
                    scanQRCode();
                }
            }else if(v==ll_keluar){
                //mambuat dialog persetujuan
                AlertDialog.Builder builder = new AlertDialog.Builder(Dashboard.this);
                builder.setTitle(getString(R.string.app_name));
                builder.setMessage("Pilih OK untuk logout.");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        keluar();
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("Kembali", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                return;
            }else if(v==ll_set_lokasi){
                cariToko();
            }
            if(open != null){
                startActivity(open);
            }
        }
    };

    public void cariToko(){
        //menambahkan data cek kehamilan terbaru
        LayoutInflater layoutInflater = (LayoutInflater)getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.cari_toko, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Dashboard.this);
        alertDialogBuilder.setView(promptView);
        final EditText et_nama_toko  = (EditText) promptView.findViewById(R.id.et_nama_toko);

        alertDialogBuilder.setCancelable(false)
                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .setPositiveButton("Cari", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String s_nama_toko = et_nama_toko.getText().toString();
                        s_nama_toko        = s_nama_toko.trim();

                        Intent i_cari_toko = new Intent(Dashboard.this, PilihToko.class);
                        i_cari_toko.putExtra("search", s_nama_toko);
                        startActivity(i_cari_toko);

                        dialog.cancel();
                    }
                });
        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    public void onRequestPermissionsResult
            (int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                Log.i("Camera", "G : " + grantResults[0]);
                // If request is cancelled, the result arrays are empty.
//                Log.i(getLocalClassName(), "grantResults.length : "+grantResults.length+" grantResults[0] :  "+grantResults[0]+"");
//                Log.i(getLocalClassName(), "PackageManager.PERMISSION_GRANTED : "+PackageManager.PERMISSION_GRANTED+" PackageManager.PERMISSION_DENIED :  "+PackageManager.PERMISSION_DENIED);

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Log.i(getLocalClassName(), "akses kamera diizinkan");
                    scanQRCode();
                    return;
                } else {
                    Log.d(getLocalClassName(), "akses kamera tidak diizinkan");

                    //It will return false if the user tapped “Never ask again”.
                    boolean is_Neveraskagain = ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.CAMERA);
                    if (is_Neveraskagain) {
                        //showAlert();
                        Log.i(getLocalClassName(), "is_Neveraskagain false");
                    } else if (!is_Neveraskagain) {
                        Log.i(getLocalClassName(), "is_Neveraskagain true");
                        if(!pref.getBoolean(GlobalConfig.PERMISIION_CAMERA_FIRST_TIME, false)){
                            //Log.i(getLocalClassName(), "PERMISIION_CAMERA_FIRST_TIME : false");
                            pref.edit().putBoolean(GlobalConfig.PERMISIION_CAMERA_FIRST_TIME, true).commit();
                        }else{
                            //Log.i(getLocalClassName(), "PERMISIION_CAMERA_FIRST_TIME : true");
                            startInstalledAppDetailsActivity(Dashboard.this);
                        }
                    }
                    return;
                }
            }
        }
    }

    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }
    private void scanQRCode(){
        IntentIntegrator integrator = new IntentIntegrator(Dashboard.this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setPrompt("Silakan arahkan ke barcode toko.");
        //integrator.setCameraId(1);  // Use a specific camera of the device
        integrator.setOrientationLocked(true);
        integrator.setBeepEnabled(true);
        integrator.setCaptureActivity(CaptureActivityPortrait.class);
        integrator.initiateScan();
    }
    private void openToko(String id_toko){      //method ini ada pada kelas PILIh Toko
        Log.d(GlobalConfig.TAG, "scan toko");
        loading = ProgressDialog.show(Dashboard.this, "Memproses data", "Mohon tunggu...", true);

        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_SCANTOKO;

        //Log.d(GlobalConfig.TAG,""+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));
            jsonBody.put(GlobalConfig.ID_TOKO, id_toko);

            //Log.d(GlobalConfig.TAG, ""+jsonBody.toString());
            request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    //Log.d(GlobalConfig.TAG, response.toString());
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            JSONObject data = response.getJSONObject("data");

                            JSONObject toko = data.getJSONObject("toko");
                            pref.edit().putBoolean(GlobalConfig.IS_TOKO, true).commit();
                            pref.edit().putString(GlobalConfig.ID_TOKO, toko.getString(GlobalConfig.ID_TOKO)).commit();
                            pref.edit().putString(GlobalConfig.GTOKO_NAMA, toko.getString(GlobalConfig.GTOKO_NAMA)).commit();
                            pref.edit().putString(GlobalConfig.GTOKO_ALAMAT, toko.getString(GlobalConfig.GTOKO_ALAMAT)).commit();
                            pref.edit().putString(GlobalConfig.GTOKO_PIMPINAN, toko.getString(GlobalConfig.GTOKO_PIMPINAN)).commit();
                            pref.edit().putString(GlobalConfig.GTOKO_NOTELP, toko.getString(GlobalConfig.GTOKO_NOTELP)).commit();
                            pref.edit().putString(GlobalConfig.GREAL_PLAFON, toko.getString(GlobalConfig.GREAL_PLAFON)).commit();
                            pref.edit().putString(GlobalConfig.GKDSALES, toko.getString(GlobalConfig.GKDSALES)).commit();
                            pref.edit().putString(GlobalConfig.GJATUHTEMPOBAYAR, toko.getString(GlobalConfig.GJATUHTEMPOBAYAR)).commit();
                            pref.edit().putString(GlobalConfig.GTOKO_LAT, toko.getString(GlobalConfig.GTOKO_LAT)).commit();
                            pref.edit().putString(GlobalConfig.GTOKO_LONG, toko.getString(GlobalConfig.GTOKO_LONG)).commit();
                            pref.edit().putString(GlobalConfig.GTOKO_NAMA_SALES, toko.getString(GlobalConfig.GTOKO_NAMA_SALES)).commit();

                           Intent i_toko = new Intent(Dashboard.this, Toko.class);
                           startActivity(i_toko);
                        }else{
                            notifikasi_dialog("",message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if(response != null && response.data != null){
                        Log.d(GlobalConfig.TAG, "code"+response.statusCode);
                        switch(response.statusCode){
                            case 404:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                displayMessage("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            case 500:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            default:
                                displayMessage("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null) {
            if(scanResult.getContents()!=null) {
                //notifikasi(scanResult.getContents());
                String id_toko = scanResult.getContents();
                openToko(id_toko);
            }else{
            }
        } else {
        }
    }
    private void keluar(){
        Log.d(GlobalConfig.TAG, "make progress dialog");
        loading = ProgressDialog.show(Dashboard.this, "Logout...", "Mohon tunggu...", true);
        String url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_LOGOUT;
        //Log.d(GlobalConfig.TAG,""+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            //Log.d(GlobalConfig.TAG, jsonBody.toString());
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    //Log.d(GlobalConfig.TAG, "tutup loading");
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            //hapus preference
                            pref.edit().remove(GlobalConfig.IS_LOGIN).commit();
                            pref.edit().remove(GlobalConfig.USER_ID).commit();
                            pref.edit().remove(GlobalConfig.USER_TOKEN).commit();
                            openLogin();
                        }else{
                            notifikasi_dialog("",message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loading.dismiss();
                    NetworkResponse response = error.networkResponse;
                    if(response != null && response.data != null){
                        Log.d(GlobalConfig.TAG, "code"+response.statusCode);
                        switch(response.statusCode){
                            case 404:
                                displayMessage("Terjadi masalah dengan server.");
                                break;
                            case 408:
                                displayMessage("Waktu terlalu lama untuk memproses, silakan ulangi lagi!");
                                break;
                            default:
                                displayMessage("Mohon maaf terjadi kesalahan.");
                                break;
                        }
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};
            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //menu.add(1, 1, 1, "Logout").setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        return super.onCreateOptionsMenu(menu);
    }
    public void displayMessage(String toastString){
        Toast.makeText(getApplicationContext(), toastString, Toast.LENGTH_LONG).show();
    }
    private void openLogin(){
        Intent login = new Intent(Dashboard.this, LoginActivity.class);
        startActivity(login);
        Dashboard.this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case 1:
                dialogLogout();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void dialogLogout(){
        AlertDialog.Builder builder = new AlertDialog.Builder(Dashboard.this);
        // Set the dialog title
        builder.setTitle(getResources().getString(R.string.app_name))
                .setMessage("Apakah yakin anda akan keluar?")
                .setCancelable(false)
                .setNegativeButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        logout();
                    }
                })
                .setPositiveButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
        ;
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void logout(){
        Log.d(GlobalConfig.TAG, "make progress dialog");
        loading = ProgressDialog.show(Dashboard.this, "Logout...", "Mohon tunggu...", true);

        String url = "";
        if(pref.getString(GlobalConfig.IP_KEY, null) != null){
            url = pref.getString(GlobalConfig.IP_KEY, "")+GlobalConfig.WEB_URL+GlobalConfig.URL_LOGOUT;
        }else{
            url = GlobalConfig.IP+GlobalConfig.WEB_URL+GlobalConfig.URL_LOGOUT;
        }
        //Log.d(GlobalConfig.TAG,""+url);
        JSONObject jsonBody;
        try {
            jsonBody = new JSONObject();
            jsonBody.put(GlobalConfig.USER_ID, pref.getString(GlobalConfig.USER_ID, ""));
            jsonBody.put(GlobalConfig.USER_TOKEN, pref.getString(GlobalConfig.USER_TOKEN, ""));

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    loading.dismiss();
                    Log.d(GlobalConfig.TAG, "tutup loading");
                    try {
                        int status      = response.getInt("status");
                        String message  = response.getString("message");
                        if(status==1){
                            //hapus semua preferences
                            pref.edit().remove(GlobalConfig.USER_ID).commit();
                            pref.edit().remove(GlobalConfig.USER_TOKEN).commit();
                            pref.edit().remove(GlobalConfig.IS_LOGIN).commit();
                        }else{
                            notifikasi_dialog("",message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //Log.d("respons",response.toString());
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    // do something
                    loading.dismiss();
                    if(error!=null) {
                        //Log.d("respons", error.getMessage().toString());
                    }
                }
            }){
                public Map<String, String> getHeaders() {
                    Map<String,String> headers = new Hashtable<String, String>();

                    //Adding parameters
                    headers.put(GlobalConfig.APP_TOKEN, GlobalConfig.APP_ID);
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }};

            request.setRetryPolicy(new DefaultRetryPolicy(
                    GlobalConfig.MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            MyAppController.getInstance().addToRequestQueue(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelRequest();
    }
    private void cancelRequest(){
        if(request!=null) {
            MyAppController.getInstance().cancelPendingRequests(request);
        }
    }
    private void notifikasi_dialog(String title, String message){
        if(title.equals("")){
            title = getResources().getString(R.string.app_name);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(Dashboard.this);
        builder.setTitle(title)
                .setCancelable(false)
                .setMessage(message)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
