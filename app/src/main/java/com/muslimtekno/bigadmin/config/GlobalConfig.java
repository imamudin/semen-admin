package com.muslimtekno.bigadmin.config;

/**
 * Created by agung on 09/03/2016.
 */
public class GlobalConfig {
    public static final String APP_ID           = "Kdia83nds9u3msdkJds";       //key untuk menerima gcm
    public static final String APP_TOKEN        = "appToken";                //key untuk mengirim app

    public static final String NAMA_PREF        = "com.imamudin.semen.pref";  //key untuk menyimpan file preferences
    //192.168.43.208
    //public static final String IP               = "http://192.168.1.23";          //alamat web/ip website
    public static final String IP               = "http://103.76.171.101";          //alamat web/ip server
    public static final String IP_KEY           = "ip";                     //key untuk alamat web/ip website
    public static final String WEB_URL          = "/apibig";                           //nama domain url


    public static final String TAG              = "mycop";                  //key untuk LOG
    public static final int MY_SOCKET_TIMEOUT_MS    = 30000;                    //30 detik

    //URL
    public static final String URL_LOGIN            = "/loginadmin";
    public static final String URL_LOGOUT           = "/logoutadmin";
    public static final String URL_SCANTOKO         = "/scantokoadmin";
    public static final String URL_UPDATEKOORDINATE = "/updatekoordinat";
    public static final String URL_TOKOS            = "/tokos_admin";


    public static final String UP_START             = "START";
    public static final String UP_LIMIT             = "LIMIT";
    public static final int MAX_ROW_PER_REQUEST     = 25;                       //jumlah baris tiap request

    //permisiion untuk marshmallow pertama kali
    public static final String PERMISIION_CAMERA_FIRST_TIME     = "PERMISIION_CAMERA_FIRST_TIME";
    public static final String PERMISIION_LOCATION_FIRST_TIME   = "PERMISIION_LOCATION_FIRST_TIME";

    //NOTIFIKASI
    public static final String notif_form_tidak_kosong  = "Username atau password tidak boleh kosong!";
    public static final String notif_form_tidak_cocok   = "Username atau password tidak sesuai!";
    public static final String notif_butuh_koneksi      = "Aplikasi membutuhkan koneksi internet!";

    public static final String USER_NAME    = "user_name";
    public static final String USER_PASWORD = "user_password";
    public static final String IS_LOGIN     = "is_login";
    public static final String USER_ID      = "id_user";
    public static final String USER_TOKEN   = "user_token";

    public static final String IS_TOKO          = "IS_TOKO";
    public static final String ID_TOKO          = "KODE";
    public static final String GTOKO_NAMA       = "NAMA";
    public static final String GTOKO_ALAMAT     = "ALAMAT";
    public static final String GREAL_PLAFON     = "REALPLAFON";
    public static final String GKDSALES         = "KDSALES";
    public static final String GJATUHTEMPOBAYAR = "JATUHTEMPOBAYAR";
    public static final String GTOKO_NOTELP     = "TELEPON";
    public static final String GTOKO_LAT        = "LATITUDE";
    public static final String GTOKO_LONG       = "LONGITUDE";
    public static final String GTOKO_NIK        = "NIK";
    public static final String GTOKO_POIN       = "POIN";
    public static final String GTOKO_PIMPINAN   = "PIMPINAN";
    public static final String GTOKO_NAMA_SALES = "NAMA_SALES";

    public static final int KODE_CARILOKASI         = 106;
    public static final String G_LATITUDE          = "LATITUDE";
    public static final String G_LONGITUDE         = "LONGITUDE";

    public static final String GT_KODE              = "KODE";
    public static final String GT_NAMA              = "NAMA";
    public static final String GT_ALAMAT            = "ALAMAT";

}
